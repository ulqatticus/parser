public class Jacket {

    String market;
    String price;

    public Jacket(String market, String price) {
        this.market = market;
        this.price = price;

    }

    public String getMarket() {
        return market;
    }

    public String getPrice() {
        return price;
    }

}
