public enum Markets {
    SPORT_MASTER("http://www.sportmaster.ua/ru/search/?search_str=+Columbia+Maguire+Place+II+"),
    ROZETKA("https://rozetka.com.ua/columbia_1619751_435_l/p21014540/"),
    PROM("https://prom.ua/Kurtka-muzhskaya-columbia.html?search_term=%D0%9A%D1%83%D1%80%D1%82%D0%BA%D0%B0+Columbia+Maguire+Place+II+");
    private String url;

    Markets(String url) {
        this.url = url;
    }

    public String url() {
        return url;
    }
}
