import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class MainParser {

    private static double PRICE_CONSTANT = 5000.00;


    public static void main(String[] args) throws IOException {
        List<Jacket> jacketList = new ArrayList<>();
        Document document;
        Elements jacketElements;
        for (Markets markets : Markets.values()) {
            if (markets.url().equals(Markets.SPORT_MASTER.url())) {
                document = Jsoup.connect(markets.url()).get();
                jacketElements = document.getElementsByAttributeValue("class", "products-list__box block_big l-goods  ");
                jacketElements.forEach(element -> {
                    Elements el = element.getElementsByAttributeValue("class", "price-new bigger");
                    el.forEach(ell -> jacketList.add(new Jacket("sport-master", ell.text().replaceAll("\\D+", ""))));
                });
            } else if (markets.url().equals(Markets.ROZETKA.url())) {
                document = Jsoup.connect(markets.url()).get();
                jacketElements = document.getElementsByAttributeValue("itemprop", "price");

                jacketElements.forEach(element -> {
                    Elements el = element.getElementsByAttribute("itemprop");
                    System.out.println();
                    String word = el.toString().replaceAll("\\D+", "");
                    jacketList.add(new Jacket("ROZETKA", word));

                });
            } else if (markets.url().equals(Markets.PROM.url())) {
                document = Jsoup.connect(markets.url()).get();
                jacketElements = document.getElementsByAttributeValue("class", "x-gallery-tile js-gallery-tile");
                jacketElements.forEach(element -> {
                    Elements el = element.getElementsByAttributeValue("class", "x-gallery-tile__price_color_red x-gallery-tile__price");
                    el.forEach(ell -> jacketList.add(new Jacket("prom-new-prices", ell.text().replaceAll("\"[^a-zA-Z0-9\\s]\"", ""))));
                    Elements el_old = element.getElementsByAttributeValue("class", "x-gallery-tile__old-price");
                    el_old.forEach(ell -> jacketList.add(new Jacket("prom-old-prices", ell.text().replaceAll("\"[^a-zA-Z0-9\\s]\"", ""))));
                    Elements el_old_p = element.getElementsByAttributeValue("class", "x-gallery-tile__price");
                    el_old_p.forEach(ell -> jacketList.add(new Jacket("prom-old-prices", ell.text().replaceAll("\"[^a-zA-Z0-9\\s]\"", ""))));
                });
            }
        }
        for (Jacket jacket : jacketList) {
            String f = jacket.getPrice().replaceAll("[грн.]+|\\u00a0", "");
            if (Double.parseDouble(f.replaceAll(",", ".")) < PRICE_CONSTANT) {
                System.out.println(jacket.getMarket() + " --- " + jacket.getPrice());
            }
        }
    }
}




